FactoryBot.define do
  factory :user do
    email { Faker::Internet.email}
    password {"123456789"}
    password_confirmation {"123456789"}
  end

#   # This will use the User class (Admin would have been guessed)
#   factory :admin, class: User do
#     first_name "Admin"
#     last_name  "User"
#     admin      true
#   end
end