# Create a new user
class UsersController < ApplicationController
  def create
    user = User.new({
                      email: user_params[:email],
                      password: user_params[:password],
                      password_confirmation: user_params[:password_confirmation]
                    })
    if user.save
      render json: user, status: :ok
    else
      render json: user.errors, status: :unprocessable_entity
    end
  end

  def show
    render json: @current_user, status: :ok
  end

  def user_params
    params.permit(
                   :email,
                   :password,
                   :password_confirmation
    )
  end
    
end