class TripsController < ApplicationController
  def create
    @trip = Trip.new(user: @current_user)
    if @trip.save!
      render json: @trip, status: :ok
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  def index
    all_trips = Trip.all.count
    
    render json: @current_user.trips.count, status: :ok
  end
end