class SessionsController < ApplicationController

  def create
    user = User.find_by_email user_params[:email]
    if user&.valid_password?(user_params[:password])
      token = JsonWebToken.encode(user.authentication_token)
      puts token
      render json: token.to_json, status: :created
    else
      head(:unauthorized)
    end
  rescue StandardError => e
    render json: e
  end

  def destroy
    pass
  end

  private
  def user_params
    params.permit(
        :email,
        :password,
        :password_confirmation
    )
  end
end