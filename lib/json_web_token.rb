class JsonWebToken
  class << self
    def encode(token, exp = 24.hours.from_now)
      payload = {}
      payload[:user_token] = token
      payload[:exp] = exp.to_i
      encoded = JWT.encode(payload, Rails.application.credentials.secret_key_base)
      puts encoded
      encoded
    end

    def decode(jwt_token)
      body = JWT.decode(jwt_token, Rails.application.credentials.secret_key_base)[0]
    rescue
      nil
    end
  end
end