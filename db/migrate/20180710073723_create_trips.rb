class CreateTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.belongs_to :user, foreign_key: true
      t.date :created_at
    end
  end
end
