# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# 4.times do 
#     User.create({
#         email:  Faker::Internet.email,
#         password: "123456789",
#         password_confirmation: "123456789"
#     })
# end


user_count = User.all.count
400.times do 
    Trip.create({
        user: User.find(Faker::Number.between(1,user_count))
    })
end

200.times do 
    Trip.create({
        user: User.find(Faker::Number.between(1,2))
    })
end

400.times do 
    Trip.create({
        user: User.find(Faker::Number.between(3,4)),
        created_at: "2018-9-20"
    })
end