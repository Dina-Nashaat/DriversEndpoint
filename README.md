# README

# How to run on local machine: 
- Make sure rails is setup on your machine
[on Windows](https://gorails.com/setup/windows/10)
[on Ubuntu](https://gorails.com/setup/ubuntu/16.04)
- Make sure Postgres is setup on your machine [How to setup postgres on ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)
- clone the repo
- if you're using rvm, make sure your ruby version is set to 2.5.1
- Run bundle install
- Run rails db:create db:migrate
- Run rails db:seed to populate the database with some records
- Run rails s to open a server on localhost:3000

# Available Endpoints for Uber Driver Service:
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/96bddbc1ce03f02fc5fa)
You could open it using postman app, or open the web view, download the json file, and import from postman on desktop.
You could also import the link directly from your Postman on desktop app.

## Sign up a new user
### POST /users
REQUEST BODY
```
{
	"email": "hello@example.com",
	"password": "123456789",
	"password_confirmation": "123456789"
}
```
RESPONSE
```
{
    "id": 2,
    "email": "dina@example.com",
    "created_at": "2018-07-10T12:01:35.079Z",
    "updated_at": "2018-07-10T12:01:35.079Z",
    "authentication_token": "dT_Fz9S3yeF3DsTtckNa"
}
```
## Login User
### POST /sessions
REQUEST BODY
```
{
	"email": "hello@example.com",
	"password": "123456789"
}
```
RESPONSE
``` 
"JWT Token"
```

## Start a trip
### POST /trips
REQUEST HEADER:
```
{
    Authorization: "bearer <token from login request>"
}
```
RESPONSE
```
{
    "id": 3,
    "user_id": 1,
    "created_at": "2018-07-10"
}
```

