Rails.application.routes.draw do
  # devise_for :users
  resources :sessions, only: [:create, :destroy]
  resources :users, only: [:create, :show]
  resources :trips, only: [:create, :index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
